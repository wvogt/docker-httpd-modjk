# Supported tags and respective `Dockerfile` links
 - `2.4.25-1.2.42`, `2.4-1.2`, `latest` ([_Dockerfile_](https://bitbucket.org/wvogt/docker-httpd-modjk/src/d22b3792875b9b856735619173e77538baa5e6ec/Dockerfile?at=master))

See the tomcat connectors page for more information about how to use and configure the mod_jk module: http://tomcat.apache.org/connectors-doc/webserver_howto/apache.html