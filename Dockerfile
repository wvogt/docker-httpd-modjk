FROM httpd:2.4

#
# Set mod_jk version
#
ENV MODJK_VERSION 1.2.42
ENV MODJK_URL https://www.apache.org/dist/tomcat/tomcat-connectors/jk/tomcat-connectors-$MODJK_VERSION-src.tar.gz

# library for mod_http2
ENV NGHTTP2_VERSION 1.18.1-1
ENV OPENSSL_VERSION 1.0.2k-1~bpo8+1

# see https://httpd.apache.org/docs/2.4/install.html#requirements
RUN set -x \
    # mod_http2 mod_lua mod_proxy_html mod_xml2enc
    # https://anonscm.debian.org/cgit/pkg-apache/apache2.git/tree/debian/control?id=adb6f181257af28ee67af15fc49d2699a0080d4c
    && buildDeps=" \
        bzip2 \
        ca-certificates \
        gcc \
        libnghttp2-dev=$NGHTTP2_VERSION \
        liblua5.2-dev \
        libpcre++-dev \
        libssl-dev=$OPENSSL_VERSION \
        libxml2-dev \
        zlib1g-dev \
        make \
        wget \
    " \
    && apt-get update \
    && apt-get install -y --no-install-recommends -V $buildDeps \
    && rm -r /var/lib/apt/lists/* \
    \
    && wget -O httpd.tar.bz2 "$HTTPD_BZ2_URL" \
    && echo "$HTTPD_SHA1 *httpd.tar.bz2" | sha1sum -c - \
    # see https://httpd.apache.org/download.cgi#verify
    && wget -O httpd.tar.bz2.asc "$HTTPD_ASC_URL" \
    && export GNUPGHOME="$(mktemp -d)" \
    && gpg --keyserver ha.pool.sks-keyservers.net --recv-keys A93D62ECC3C8EA12DB220EC934EA76E6791485A8 \
    && gpg --batch --verify httpd.tar.bz2.asc httpd.tar.bz2 \
    && rm -r "$GNUPGHOME" httpd.tar.bz2.asc \
    \
    && mkdir -p src \
    && tar -xf httpd.tar.bz2 -C src --strip-components=1 \
    && rm httpd.tar.bz2 \
    # build mod_jk
    && wget -O mod_jk.tar.gz "$MODJK_URL" \
    && wget -O mod_jk.tar.gz.asc "$MODJK_URL.asc" \
    && mkdir -p src/mod_jk \
    && tar -xvf mod_jk.tar.gz -C src/mod_jk --strip-components=1 \
    && rm mod_jk.tar.gz* \
    && cd src/mod_jk/native \
    && ./configure --with-apxs=/usr/local/apache2/bin/apxs \
    && make \
    && cp apache-2.0/mod_jk.so /usr/local/apache2/modules/mod_jk.so \
    && cd ../../.. \
    && rm -r src \
    && apt-get purge -y --auto-remove $buildDeps

# Load the module in httpd.conf
RUN printf 'LoadModule    jk_module  modules/mod_jk.so\n\n' >> /usr/local/apache2/conf/httpd.conf
